# diagram.py
from diagrams import Cluster, Diagram
from diagrams.onprem.network import Nginx
from diagrams.onprem.container import Docker
from diagrams.programming.framework import Laravel
from diagrams.onprem.inmemory import Redis
from diagrams.onprem.database import PostgreSQL
from diagrams.custom import Custom
from diagrams.oci.storage import ObjectStorage
from diagrams.saas.cdn import Cloudflare
from diagrams.onprem.monitoring import Sentry

with Diagram("Infrastructure", show=False, outformat="pdf"):
    with Cluster("Scaleway"):
        with Cluster("VPS Scaleway"):
            nginx = Nginx('Load balancer')

            with Cluster("Docker"):
                docker = Docker("Docker")

                onlyfrogz = Laravel("OnlyFrogz.com")
                postgres = PostgreSQL("PostgreSQL")
                redis = Redis("Redis")

                meilisearch = Custom(
                    "MeiliSearch", "meilisearch.png")

            nginx >> docker >> onlyfrogz
            onlyfrogz >> postgres
            onlyfrogz >> redis
            onlyfrogz >> meilisearch

        os = ObjectStorage("Stockage objet")

        onlyfrogz >> os

    cloudflare = Cloudflare("Cloudflare")
    cloudflare >> nginx

    client = Custom("Utilisateur", "pc.png")

    sendgrid = Custom("SendGrid", "sendgrid.png")
    sentry = Sentry("Sentry")

    onlyfrogz >> sendgrid
    onlyfrogz >> sentry

    client >> cloudflare
    client >> os
