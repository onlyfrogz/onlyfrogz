<?php

use App\Http\Controllers\LikeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\TagController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:sanctum')->post('/post/{post}/like', [
    LikeController::class, 'likePost'
]);

Route::middleware('auth:sanctum')->post('/comment/{comment}/like', [
    LikeController::class, 'likeComment'
]);

Route::middleware('auth:sanctum')->post('/user/{user}/unsubscribe', [
    SubscriptionController::class, 'unsubscribe'
]);

Route::middleware('auth:sanctum')->post('/user/{user}/subscribe', [
    SubscriptionController::class, 'subscribe'
]);

Route::middleware('auth:sanctum')->delete('/post/{post}', [
    PostController::class, 'deletePost'
]);

Route::middleware('auth:sanctum')->get('/tags', [TagController::class, 'searchTags']);

Route::middleware('auth:sanctum')->get('/search', [SearchController::class, 'searchAll']);
