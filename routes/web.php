<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return redirect('feed');
    } else {
        return view('welcome');
    }
});

Route::get('/auth/google', [AuthController::class, 'googleRedirect']);
Route::get('/auth/google/callback', [AuthController::class, 'handleGoogleLogin']);
Route::get('/auth/facebook', [AuthController::class, 'facebookRedirect']);
Route::get('/auth/facebook/callback', [AuthController::class, 'handleFacebookLogin']);
Route::get('/auth/twitter', [AuthController::class, 'twitterRedirect']);
Route::get('/auth/twitter/callback', [AuthController::class, 'handleTwitterLogin']);

Route::get('/feed', [PostController::class, 'feedView'])->name('feed')->middleware('auth');

Route::get('/discover', function () {
    return view('discover');
})->name('discover')->middleware('auth');

Route::get('/publish', [PostController::class, 'publishView'])->name('publish')->middleware('auth');

Route::post('/publish', [PostController::class, 'store'])->middleware('auth');

Route::get('/discover', [PostController::class, 'discoverView'])->name('discover')->middleware('auth');

Route::get('/user/{user}', [UserController::class, 'userView'])->name('user')->middleware('auth');

Route::get('/post/{post}', [PostController::class, 'postView'])->name('post')->middleware('auth');

Route::post('/post/{post}/comment', [CommentController::class, 'store'])->name('comment')->middleware('auth');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
