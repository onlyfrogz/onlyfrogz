require('./bootstrap');
import '@fortawesome/fontawesome-free/css/all.css';
import Alpine from 'alpinejs';

window.Alpine = Alpine;
Alpine.start();
import Swiper from './swiper'
import './publish'
import './search'
import './user'
import './post'
import {updateStar} from "./common";

const copyToClipboard = str => {
    const el = document.createElement('textarea');
    el.value = str;
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);
};

document.querySelectorAll(".swiper-slide video").forEach(elm => {
    elm.volume = 0.4
});

document.querySelectorAll('.swiper-container:not(.override)').forEach((el) => {
    new Swiper(el, {
        // Optional parameters
        direction: 'horizontal',
        loop: true,

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        mousewheel: true,

        on: {
            slideChangeTransitionEnd: (swiper) => {
                swiper.slides.forEach(elm => {
                    const video = elm.querySelector('video')
                    if (video) video.pause()
                })
                const newSlide = swiper.slides[swiper.activeIndex]?.querySelector('video')
                if (newSlide && swiper.previousIndex > 0) newSlide.play()
            }
        }

    });
});

document.querySelectorAll('.onlyfrogzLikePost').forEach((el) => {
    el.addEventListener('click', () => {
        const icon = el.querySelector('i');
        const liked = (icon.classList.contains('fas'));
        axios.post(`/api/post/${el.dataset.postId}/like`, {direction: liked ? 'unlike' : 'like'});
        updateStar(liked, icon, el);
    });
});

document.querySelectorAll('.onlyfrogzUnsubscribe').forEach((el) => {
    el.addEventListener('click', () => {
        const confirm = window.confirm(messages.confirmUnsubscribe);
        if (confirm) {
            axios.post(`/api/user/${Number(el.dataset.contentCreatorId)}/unsubscribe`).then(() => {
                document.location.reload();
            });
        }
    });
});

document.querySelectorAll('.onlyfrogzShare').forEach((el) => {
    el.addEventListener('click', () => {
        copyToClipboard(`${window.location.origin}/post/${el.dataset.postId}`);
        alert(messages.clipboard);
    })
});
