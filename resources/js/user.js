document.querySelector('.onlyfrogzConfirmSubscription')?.addEventListener('click', (el) => {
    const confirm = window.confirm('Êtes-vous sûr de vouloir vous abonner à ce créateur de contenu ?');
    const months = Number(document.querySelector('select[name=onlyfrogzSubscribeSelect]').value);
    if (confirm) {
        axios.post(`/api/user/${Number(el.target.dataset.id)}/subscribe`, {months}).then(() => {
            document.location.reload();
        });
    }
});
