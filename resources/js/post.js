import Swiper from './swiper'
import {updateStar} from "./common";

new Swiper(".onlyfrogzOnePostSwiper", {
    slidesPerView: "auto",
    spaceBetween: 30,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

    mousewheel: true
});

document.querySelectorAll('.onlyfrogzCommentLike').forEach((el) => {
    el.addEventListener('click', () => {
        const icon = el.querySelector('i');
        const liked = (icon.classList.contains('fas'));
        axios.post(`/api/comment/${el.dataset.commentId}/like`, {direction: liked ? 'unlike' : 'like'});
        updateStar(liked, icon, el);
    });
});
