export function updateStar(liked, icon, el) {
    if (liked) {
        icon.classList.remove('fas');
        icon.classList.add('far');
        el.childNodes[0].nodeValue = Number(el.textContent) - 1 + " ";
    } else {
        icon.classList.remove('far');
        icon.classList.add('fas');
        el.childNodes[0].nodeValue = Number(el.textContent) + 1 + " ";
    }
}
