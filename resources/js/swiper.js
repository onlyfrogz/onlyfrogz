import Swiper from 'swiper';
import 'swiper/swiper-bundle.css';
import SwiperCore, {Navigation, Pagination, Mousewheel} from 'swiper/core';
SwiperCore.use([Navigation, Pagination, Mousewheel]);
export default Swiper
