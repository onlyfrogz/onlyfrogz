import { debounce } from "lodash"

let results = { posts: [], users: [] }

const searchResults = document.querySelector('.onlyfrogzSearchResults')

const buildHTML = () => {
    const posts = results.posts
    const users = results.users

    searchResults.innerHTML = ""

    searchResults.insertAdjacentHTML("beforeend", `
            ${posts.length === 0 && users.length === 0 ? "Aucun résultat" : ""}
            ${posts.length > 0 ?
            `<h2>Posts</h2>
                <div class="onlyfrogzSearchPosts">
                    ${posts.map(post => `
                    <a href="/post/${post.id}">
                        <article>
                            ${post.contents[0].type === "photo" ? `<img src="${post.contents[0]?.url}" alt=""/>` : `<video autoplay muted loop src="${post.contents[0]?.url}"></video>`}
                            <h3>${post.title}</h3>
                        </article>
                    </a>
                    `).join("")}
                </div>` : ""}
            ${users.length > 0 ?
            `<h2>Créateurs</h2>
                <div class="onlyfrogzSearchUsers">
                    ${users.map(user => `
                    <a href="/user/${user.id}">
                        <article>
                            <img src="${user.profile_photo_url}"/>
                            <h3>${user.name}</h3>
                        </article>
                    </a>
                    `).join("")}
                </div>` : ""}
        `)
    toggleDisplay(true)
}

const toggleDisplay = (forceState) => {
    if (forceState === true) searchResults?.classList.add("shown")
    else if (forceState === false) searchResults?.classList.remove("shown")
    else searchResults?.classList.toggle("shown")
}

const debouncedSearch = debounce((evt) => {
    const query = evt.target.value
    if (!query) {
        toggleDisplay(false)
        return
    }
    axios.get("/api/search", { params: { q: query } }).then(result => {
        results = result.data
        buildHTML()
        toggleDisplay(true)
    })
}, 200, {
    trailing: true
})

const searchBar = document.querySelector('.onlyfrogzSearchbar')
searchBar?.addEventListener("keydown", debouncedSearch)
searchBar?.addEventListener("click", (e) => {
    e.stopPropagation()
    if (searchBar.value)
        toggleDisplay(true)
})
searchResults?.addEventListener("click", (e) => e.stopPropagation())
window.addEventListener("click", () => toggleDisplay(false))
