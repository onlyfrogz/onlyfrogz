import Tagify from '@yaireo/tagify'
import '@yaireo/tagify/dist/tagify.css'
import * as FilePond from 'filepond';
import 'filepond/dist/filepond.min.css';
import 'filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css';
import FilePondPluginFileValidateSize from 'filepond-plugin-file-validate-size';
import FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import {debounce} from "lodash";

FilePond.registerPlugin(FilePondPluginFileValidateSize);
FilePond.registerPlugin(FilePondPluginFileValidateType);
FilePond.registerPlugin(FilePondPluginImagePreview);

const inputTag = document.querySelector('.onlyfrogzUploadTags');
let tagify;
if (inputTag) {
    tagify = new Tagify(inputTag, {whitelist: []});
    tagify.on('input', debounce(onInput, 300));
}

function onInput(e) {
    const value = e.detail.value
    tagify.whitelist = null
    tagify.loading(true).dropdown.hide()
    axios.get('/api/tags', {
        params: {
            tagName: value
        }
    }).then((res) => {
        tagify.whitelist = res.data.map((tag) => {
            return tag.name;
        });
        tagify.loading(false).dropdown.show(value);
    }).catch(console.error);
}

document.querySelectorAll('.onlyfrogzDeletePostButton').forEach((el) => {
    el.addEventListener('click', (event) => {
        event.stopPropagation();
        event.preventDefault();
        const confirm = window.confirm(messages.confirmDelete);
        if (confirm) {
            axios.delete(`/api/post/${Number(el.dataset.postId)}`).then(() => {
                document.location.reload();
            });
        }
    });
});

const checkbox = document.querySelector('.onlyfrogzUploadCheckbox');
const star = document.querySelector('.onlyfrogzUploadPremium');

if (star) {
    star.addEventListener('click', () => {
        const premium = (star.classList.contains('fas'));
        if (premium) {
            resetPremium(star, checkbox)
        } else {
            star.classList.remove('far');
            star.classList.add('fas');
            checkbox.checked = true;
        }
    });
}
let filepond;
const file = document.querySelector('.onlyfrogzFileUploader');
if (file) {
    filepond = FilePond.create(file, {
        allowMultiple: true,
        allowReorder: true,
        storeAsFile: true,
        maxFiles: 5,
        credits: {},
        maxTotalFileSize: '99MB',
        acceptedFileTypes: ['image/*', 'video/*', 'video/mp4', 'video/mpeg', 'video/webm', 'image/webp'],
        imagePreviewMaxHeight: 180
    });
}

const reset = document.querySelector('.onlyfrogzCancelUpload');
if (reset) {
    reset.addEventListener('click', () => {
        filepond.removeFiles();
        tagify.removeAllTags();
        resetPremium(star, checkbox);
    });
}

function resetPremium(star, checkbox) {
    star.classList.remove('fas');
    star.classList.add('far');
    checkbox.checked = false;
}

document.querySelectorAll('.onlyfrogzPostPublish').forEach(el => {
    const slides = el.querySelector(".onlyfrogzPostSlidePublish")
    const button = el.querySelector(".onlyfrogzDeletePostButton")
    el.addEventListener('mouseover', evt => {
        evt.stopPropagation()
        slides.classList.add('hovered');
        button.classList.add('hovered');
    });
    el.addEventListener('mouseout', evt => {
        evt.stopPropagation()
        slides.classList.remove('hovered');
        button.classList.remove('hovered');
    });
});


