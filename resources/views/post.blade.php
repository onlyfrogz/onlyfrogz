@extends('layouts.onlyfrogz')
@section('title',__('onlyfrogz.post',['name'=>$post->title]))
@section('content')
    <div class="onlyfrogzPostPageContent">
        <header>
            <?php
            $hasAccess = !$post->premium || $hasValidSubscription || $post->user->id == Auth::user()->id;
            ?>
            <div class="onlyfrogzUserInfoContainer">
                <div class="onlyfrogzCircleAvatar onlyfrogzCircleAvatarPost">
                    <a href="{{route('user',$post->user)}}"><img src="{{$post->user->profile_photo_url}}"
                                                                 alt="{{__('onlyfrogz.altprofilepicture',['name' => $post->user->name])}}"></a>
                    <div class="onlyfrogzUserInfoPanel">
                        <div>
                            <h3>{{$post->user->name}}</h3>
                            <span><b>{{$post->user->subscribers_count}} </b>{{trans_choice('onlyfrogz.subscribers',$post->user->subscribers_count)}}</span>
                            <span><b>{{$post->user->posts_count}} </b>{{trans_choice('onlyfrogz.posts', $post->user->posts_count)}}</span>
                            @if($post->user->subscribers->count() > 0 && $post->user->id != Auth::user()->id)
                                <button data-content-creator-id="{{$post->user->id}}"
                                        class="onlyfrogzUnsubscribe">{{__('onlyfrogz.unsubscribe')}}</button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="onlyfrogzTags">
                    @foreach($post->tags as $tag)
                        <span style="background-color: {{$tag->color}}">{{$tag->name}}</span>
                    @endforeach
                    <h2 class="onlyfrogzPostTitle">{{$post->title}}
                        @if($post->premium)
                            <i class="fas fa-star fa-1x"></i>
                        @endif
                    </h2>
                </div>
            </div>
            <div class="onlyfrogzPostTextContainer onlyfrogzPostPageDescription">
                @if($hasAccess)
                    <p class="onlyfrogzPostDescription">{{$post->description}}</p>
                @else
                    <p class="onlyfrogzPostPremiumDescription">{{__('onlyfrogz.premium', ['name' => $post->user->name])}}</p>
                @endif
            </div>
        </header>
        <div class="onlyfrogzUserAllPosts">
            <article>
                <div class="override onlyfrogzOnePostSwiper swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($post->contents as $content)
                            <div class="swiper-slide">
                                @switch($content->type)
                                    @case("video")
                                    <video src="{{$content->url}}"
                                           style="filter:{{ !$hasAccess ? 'blur(10px) saturate(0.3)': 'none'}};"
                                           {{!$hasAccess && "muted"}} controls>
                                        @break
                                        @case("photo")
                                        <img src="{{$content->url}}" alt=""
                                             style="filter:{{ !$hasAccess ? 'blur(10px) saturate(0.3)': 'none'}};">
                                    @break
                                @endswitch
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <div class="onlyfrogzLikeAndSharePost">
                    @if($hasAccess)
                        <button data-post-id="{{$post->id}}" class="onlyfrogzLikePost">{{$post->likes_count}} <i
                                class="{{$post->likes->count() > 0 ? "fas" : "far"}} fa-heart"></i></button>
                        <button data-post-id="{{$post->id}}" class="onlyfrogzShare"><i class="fas fa-share-alt"></i>
                        </button>
                    @endif
                </div>
            </article>
        </div>
        @if($hasAccess)
            <h2>{{trans_choice('onlyfrogz.comments',$post->comments_count)}}</h2>
            <div class="onlyfrogzComment">
                <div class="onlyfrogzCircleAvatar onlyfrogzCircleAvatarPost">
                    <img src="{{Auth::user()->profile_photo_url}}"
                         alt="{{__('onlyfrogz.altprofilepicture',['name' => Auth::user()->name])}}"></a>
                </div>
                <form method="POST" action="{{route('comment',['post' => $post])}}" class="onlyfrogzCommentForm">
                    @csrf
                    <textarea required class="onlyfrogzCommentContent onlyfrogzPostComment" maxlength="500"
                              name="comment"
                              rows="5" cols="50" placeholder="{{__('onlyfrogz.inputcommentdescription')}}"></textarea>
                    <div class="onlyfrogzCommentButtons">
                        <button type="reset" class="onlyfrogzCancelUpload">{{__('onlyfrogz.cancel')}}</button>
                        <input class="onlyfrogzConfirmUpload" type="submit" value="{{__('onlyfrogz.send')}}">
                    </div>
                </form>
            </div>
            @foreach($post->comments as $comment)
                <div class="onlyfrogzComment">
                    <div class="onlyfrogzCircleAvatar onlyfrogzCircleAvatarPost">
                        <a href="{{route('user',$comment->user)}}"><img src="{{$comment->user->profile_photo_url}}"
                                                                        alt="{{__('onlyfrogz.altprofilepicture',['name' => $comment->user->name])}}"></a>
                        <div class="onlyfrogzUserInfoPanel">
                            <div>
                                <h3>{{$comment->user->name}}</h3>
                                <span><b>{{$comment->user->subscribers_count}} </b>{{trans_choice('onlyfrogz.subscribers',$comment->user->subscribers_count)}}</span>
                                <span><b>{{$comment->user->posts_count}} </b>{{trans_choice('onlyfrogz.posts', $comment->user->posts_count)}}</span>
                                @if($comment->user->subscribers->count() > 0)
                                    <button data-content-creator-id="{{$post->user->id}}"
                                            class="onlyfrogzUnsubscribe">{{__('onlyfrogz.unsubscribe')}}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div style="background-color: {{$loop->index % 2 == 0 ? '#EEF1ED': '#D9E6D7'}}"
                         class="onlyfrogzCommentContent">
                        <h3><b>{{$comment->user->name}}</b> - {{$comment->created_at}}</h3>
                        <p>{{$comment->content}}</p>
                        <button data-comment-id="{{$comment->id}}"
                                class="onlyfrogzLike onlyfrogzCommentLike">{{$comment->likes_count}} <i
                                class="{{$comment->likes->count() > 0 ? "fas" : "far"}} fa-heart"></i></button>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
@endsection
