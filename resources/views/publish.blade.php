@extends('layouts.onlyfrogz')
@section('title',__('onlyfrogz.publish'))
@section('content')
    <header>
        <h1 class="onlyfrogzPageTitle">{{__('onlyfrogz.publish')}}</h1>
    </header>
    <form enctype="multipart/form-data" method="POST" action="/publish" class="onlyfrogzUploadContainer">
        @csrf
        <div class="onlyfrogzUploadLeftPanel">
            <input required type="file" name="file[]" class="onlyfrogzFileUploader">
            <div class="onlyfrogzUploadButtons">
                <input type="submit" class="onlyfrogzConfirmUpload" value="{{__('onlyfrogz.confirm')}}">
                <button type="reset" class="onlyfrogzCancelUpload">{{__('onlyfrogz.cancel')}}</button>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
        <div class="onlyfrogzUploadRightPanel">
            <textarea required class="onlyfrogzUploadPostDescription" maxlength="500" name="description"
                      rows="6" cols="50" placeholder="{{__('onlyfrogz.inputpostdescription')}}"></textarea>
            <input placeholder="{{__('onlyfrogz.inputposttag')}}" name="tags"
                   class="onlyfrogzUploadTags">
            <input required type="text" class="onlyfrogzUploadPostTitle" maxlength="60" name="title"
                   placeholder="{{__('onlyfrogz.inputposttitle')}}">
            <input name="premium" class="onlyfrogzUploadCheckbox" type="checkbox"><i
                title="{{__('onlyfrogz.startooltip')}}"
                class="onlyfrogzUploadPremium far fa-2x fa-star"></i>
        </div>
    </form>
    <h1 class="onlyfrogzPageTitle">{{__('onlyfrogz.lastposts')}}</h1>
    @if($posts->count() == 0)
        <h2 class="onlyfrogzEmptyFeed onlyfrogzEmptyLastPosts">{{__('onlyfrogz.emptylastposts')}}</h2>
    @endif
    <div class="onlyfrogzPersonnalPostContainer">
        @foreach($posts as $post)
            <article class="onlyfrogzPostPublish">
                <a href="{{route('post',$post)}}">
                    <h3 class="onlyfrogzPostTitlePublish">{{$post->title}}</h3>
                    <div class="swiper-container onlyfrogzPostSlidePublish">
                        <div class="swiper-wrapper">
                            @foreach($post->contents as $content)
                                <div class="swiper-slide">
                                    @switch($content->type)
                                        @case("video")
                                        <video src="{{$content->url}}" muted controls>
                                            @break
                                            @case("photo")
                                            <img src="{{$content->url}}" alt="">
                                            @break
                                            @endswitch
                                            <img src="{{$content->url}}" alt="">
                                </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination"></div>
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <button data-post-id="{{$post->id}}"
                            class="onlyfrogzCancelUpload onlyfrogzDeletePostButton">{{__('onlyfrogz.deletepost')}} <i
                            class="far fa-trash-alt"></i>
                    </button>
                </a>
            </article>
        @endforeach
    </div>
@endsection
