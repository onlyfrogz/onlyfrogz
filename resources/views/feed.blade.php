@extends('layouts.onlyfrogz')
@section('title',__('onlyfrogz.feed'))
@section('content')
    <header>
        <h1 class="onlyfrogzPageTitle">{{__('onlyfrogz.feed')}}</h1>
    </header>
    @if($posts->count() == 0)
        <h2 class="onlyfrogzEmptyFeed">{{__('onlyfrogz.emptyfeedfirstpart')}}<a
                href="{{route('discover')}}">{{__('onlyfrogz.emptyfeedlink')}}</a><span>{{__('onlyfrogz.emptyfeedlastpart')}}</span>
        </h2>
    @endif
    <div class="onlyfrogzAllPostsFeed">
        @foreach($posts as $post)
            <article class="onlyfrogzPostContainer">
                <?php
                $author = $subscriptions->find($post->user);
                $hasAccess = !($post->premium && $author->pivot->expires_at < \Carbon\Carbon::now());
                ?>
                <div class="onlyfrogzAvatarAndSocialContainer">
                    <div class="onlyfrogzCircleAvatar">
                        <a href="{{route('user',$author)}}">
                            <img src="{{$author->profile_photo_url}}"
                                 alt="{{__('onlyfrogz.altprofilepicture',['name' => $post->user->name])}}">
                        </a>
                        <div class="onlyfrogzUserInfoPanel">
                            <div>
                                <h3>{{$author->name}}</h3>
                                <span><b>{{$author->subscribers_count}} </b>{{trans_choice('onlyfrogz.subscribers',$author->subscribers_count)}}</span>
                                <span><b>{{$author->posts_count}} </b>{{trans_choice('onlyfrogz.posts', $author->posts_count)}}</span>
                                @if($post->user->subscribers->count() > 0)
                                    <button data-content-creator-id="{{$post->user->id}}"
                                            class="onlyfrogzUnsubscribe">{{__('onlyfrogz.unsubscribe')}}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="onlyfrogzSocial">
                        @if($hasAccess)
                            <button data-post-id="{{$post->id}}" class="onlyfrogzLike onlyfrogzLikePost">{{$post->likes_count}} <i
                                    class="{{$post->likes->count() > 0 ? "fas" : "far"}} fa-heart"></i></button>
                            <span>{{$post->comments_count}} <i class="far fa-comments"></i></span>
                            <button data-post-id="{{$post->id}}" class="onlyfrogzShare"><i
                                    class="fas fa-share-alt"></i>
                            </button>
                        @endif
                    </div>
                </div>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($post->contents as $content)
                            <div class="swiper-slide">
                                @switch($content->type)
                                    @case("video")
                                    <video src="{{$content->url}}"
                                           style="filter:{{ !$hasAccess ? 'blur(10px) saturate(0.3)': 'none'}};"
                                           {{!$hasAccess && "muted"}} controls>
                                        @break
                                        @case("photo")
                                        <img src="{{$content->url}}" alt=""
                                             style="filter:{{ !$hasAccess ? 'blur(10px) saturate(0.3)': 'none'}};">
                                    @break
                                @endswitch
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <a href="{{route('post',$post)}}">
                    <div class="onlyfrogzPostTextContainer">
                        @if($hasAccess)
                            <p class="onlyfrogzPostDescription">{{Str::limit($post->description,200)}}</p>
                        @else
                            <p class="onlyfrogzPostPremiumDescription">{{__('onlyfrogz.premium', ['name' => $post->user->name])}}</p>
                        @endif
                        <div>
                            <div class="onlyfrogzTags">
                                @foreach($post->tags as $tag)
                                    <span style="background-color: {{$tag->color}}">{{$tag->name}}</span>
                                @endforeach
                            </div>
                            <h2 class="onlyfrogzPostTitle">{{$post->title}}
                                @if($post->premium)
                                    <i class="fas fa-star"></i>
                                @endif
                            </h2>
                        </div>
                    </div>
                </a>
            </article>
        @endforeach
    </div>
@endsection
