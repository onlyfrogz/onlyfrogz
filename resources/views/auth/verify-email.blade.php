<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <a href="/"><h1 class="onlyfrogzTitleForm">OnlyFrogz</h1></a>
        </x-slot>

        <div class="mb-4 text-sm text-gray-600">
            {{ __('onlyfrogz.thanks')}}        </div>

        @if (session('status') == 'verification-link-sent')
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ __('onlyfrogz.linksent') }}
            </div>
        @endif

        <div class="mt-4 flex items-center justify-between">
            <form method="POST" action="{{ route('verification.send') }}">
                @csrf

                <div>
                    <x-jet-button type="submit">
                        {{ __('onlyfrogz.resendemail') }}
                    </x-jet-button>
                </div>
            </form>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <button type="submit" class="underline text-sm text-gray-600 hover:text-gray-900">
                    {{ __('onlyfrogz.disconnect') }}
                </button>
            </form>
        </div>
    </x-jet-authentication-card>
</x-guest-layout>
