@section('title',__('onlyfrogz.login'))

<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <a href="/"><h1 class="onlyfrogzTitleForm">OnlyFrogz</h1></a>
        </x-slot>

        <x-jet-validation-errors class="mb-4"/>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('onlyfrogz.email') }}"/>
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"
                             required autofocus/>
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('onlyfrogz.password') }}"/>
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required
                             autocomplete="current-password"/>
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember"/>
                    <span class="ml-2 text-sm text-gray-600">{{ __('onlyfrogz.remember') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="text-sm text-gray-600 hover:text-gray-900 onlyfrogzForgotPasswordButtonLogin"
                       href="{{ route('password.request') }}">
                        {{ __('onlyfrogz.passwordlost') }}
                    </a>
                @endif

                <x-jet-button class="onlyfrogzButtonForm ml-4">
                    {{ __('onlyfrogz.connection') }}
                </x-jet-button>
            </div>

            <div class="flex items-center justify-end mt-8">
                <a class="btn" href="{{ url('auth/facebook') }}"
                style="background: #3B5499; color: #ffffff; padding: 10px; width: 100%; text-align: center; display: block; border-radius:3px;">
                {{__('onlyfrogz.loginWithFacebook')}}
            </a>
            </div>
            <div class="flex items-center justify-end mt-4">
                <a class="btn" href="{{ url('auth/google') }}"
                    style="background: #ac3333; color: #ffffff; padding: 10px; width: 100%; text-align: center; display: block; border-radius:3px;">
                    {{__('onlyfrogz.loginWithGoogle')}}
                </a>
            </div>
            <div class="flex items-center justify-end mt-4">
                <a class="btn" href="{{ url('auth/twitter') }}"
                    style="background: #4ec1e3; color: #ffffff; padding: 10px; width: 100%; text-align: center; display: block; border-radius:3px;">
                    {{__('onlyfrogz.loginWithTwitter')}}
                </a>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
