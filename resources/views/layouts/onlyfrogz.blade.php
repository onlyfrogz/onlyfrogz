<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>OnlyFrogz / @yield('title')</title>

    <link rel="stylesheet" href={{ mix('css/app.css') }}>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Allura&family=Ubuntu&display=swap" rel="stylesheet">

</head>
<body>
<nav class="onlyfrogzContainer onlyfrogzNav">
    <div class="onlyfrogzNavLogoAndSearchbar">
        <a href="/">
            <h1 class="onlyfrogzNavLogo">
                OnlyFrogz
            </h1>
        </a>
        @auth
            <div class=onlyfrogzSearchContainer>
                <input class="onlyfrogzSearchbar" type="text"
                       placeholder="{{__('onlyfrogz.search')}}">
                <div class="onlyfrogzSearchResults"></div>
            </div>
        @endauth
    </div>
    @guest
        <a href="{{route('login')}}">
            <button class="onlyfrogzNavButton onlyfrogzConnectionButton">
                {{__('onlyfrogz.connection')}}
            </button>
        </a>
    @endguest
    @auth
        <div class="onlyfrogzNavContainer">
            <a href="{{route('feed')}}">
                <button class="onlyfrogzNavButton onlyfrogzFeedButton">
                    {{__('onlyfrogz.feed')}}
                </button>
            </a>
            <a href="{{route('discover')}}">
                <button class="onlyfrogzNavButton onlyfrogzDiscoverButton">
                    {{__('onlyfrogz.discover')}}
                </button>
            </a>
            <a href="{{route('publish')}}">
                <button class="onlyfrogzNavButton onlyfrogzPublishButton">
                    {{__('onlyfrogz.publish')}}
                </button>
            </a>
            <div class="onlyfrogzAvatarWrapper">
                <div class="onlyfrogzAvatarContainer">
                    <span>{{Auth::user()->balance}} 🐸</span>
                    <button class="onlyfrogzProfileButton">
                        <a href="{{route('user',Auth::user())}}">
                            <img src="{{Auth::user()->profile_photo_url}}"
                                 alt="{{__('onlyfrogz.altprofilepicture',['name' => Auth::user()->name])}}">
                        </a>
                    </button>
                </div>
                <div class="onlyfrogzAvatarSubmenu">
                    <a href="{{ route('profile.show') }}">{{__('onlyfrogz.settings')}}</a>
                    <a href="{{ route('logout') }}" class="alert"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        {{__('onlyfrogz.disconnect')}}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
        </div>
    @endauth
</nav>
<div class="onlyfrogzContent {{$wrapperClassname ?? ''}}">
    @yield('content')
</div>
<script>
    var messages = {
        'clipboard': "{{__('onlyfrogz.clipboard')}}",
        'confirmUnsubscribe': "{{__('onlyfrogz.confirmUnsubscribe')}}",
        'confirmDelete': "{{__('onlyfrogz.confirmDelete')}}"
    }
</script>
<script src={{ mix('js/app.js') }}></script>
</body>
</html>
