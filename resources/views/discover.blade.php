@extends('layouts.onlyfrogz')
@section('title',__('onlyfrogz.discover'))
@section('content')
    <header>
        <h1 class="onlyfrogzPageTitle">{{__('onlyfrogz.discover')}}</h1>
    </header>
    <section class="onlyfrogzPopularTags">
        @foreach($tags as $tag)
            <span class="onlyfrogzTag" style="background-color: {{$tag->color}}">{{$tag->name}}
            </span>
            <div class="onlyfrogzPostsPerTag">
                @foreach($tag->tenPosts as $post)
                    <div class="onlyfrogzPostPerTag">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                @foreach($post->contents as $content)
                                    <div class="swiper-slide">
                                        @switch($content->type)
                                            @case("video")
                                            <video src="{{$content->url}}" controls>
                                                @break
                                                @case("photo")
                                                <img src="{{$content->url}}">
                                            @break
                                        @endswitch
                                    </div>
                                @endforeach
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <div class="onlyfrogzPostDiscover">
                            <div class="onlyfrogzCircleAvatar onlyfrogzCircleAvatarDiscover">
                                <a href="{{route('user',$post->user)}}">
                                    <img src="{{$post->user->profile_photo_url}}"
                                         alt="{{__('onlyfrogz.altprofilepicture',['name' => $post->user->name])}}">
                                </a>
                                <div class="onlyfrogzUserInfoPanel onlyfrogzUserInfoPanelDiscover">
                                    <div>
                                        <h3>{{$post->user->name}}</h3>
                                        <span><b>{{$post->user->subscribers_count}} </b>{{trans_choice('onlyfrogz.subscribers',$post->user->subscribers_count)}}</span>
                                        <span><b>{{$post->user->posts_count}} </b>{{trans_choice('onlyfrogz.posts', $post->user->posts_count)}}</span>
                                        @if($post->user->subscribers->count() > 0)
                                            <button data-content-creator-id="{{$post->user->id}}"
                                                    class="onlyfrogzUnsubscribe">{{__('onlyfrogz.unsubscribe')}}</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('post',$post)}}">
                                <h2 class="onlyfrogzPostTitleDiscover">{{$post->title}}</h2>
                            </a>
                            <div class="onlyfrogzSocial">
                                <button data-post-id="{{$post->id}}" class="onlyfrogzLikePost">{{$post->likes_count}} <i
                                        class="{{$post->likes->count() > 0 ? "fas" : "far"}} fa-heart"></i></button>
                                <span>{{$post->comments_count}} <i class="far fa-comments"></i></span>
                                <button data-post-id="{{$post->id}}" class="onlyfrogzShare"><i
                                        class="fas fa-share-alt"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    </a>
                @endforeach
            </div>
        @endforeach
    </section>
    <h1 class="onlyfrogzPageTitle">{{__('onlyfrogz.recommended')}}</h1>
    <div class="onlyfrogzAllPostsFeed">
        @foreach($recommendedPosts as $post)
            <article class="onlyfrogzPostContainer">
                <div class="onlyfrogzAvatarAndSocialContainer">
                    <div class="onlyfrogzCircleAvatar">
                        <a href="{{route('user',$post->user)}}">
                            <img src="{{$post->user->profile_photo_url}}"
                                 alt="{{__('onlyfrogz.altprofilepicture',['name' => $post->user->name])}}">
                        </a>
                        <div class="onlyfrogzUserInfoPanel">
                            <div>
                                <h3>{{$post->user->name}}</h3>
                                <span><b>{{$post->user->subscribers_count}} </b>{{trans_choice('onlyfrogz.subscribers',$post->user->subscribers_count)}}</span>
                                <span><b>{{$post->user->posts_count}} </b>{{trans_choice('onlyfrogz.posts', $post->user->posts_count)}}</span>
                                @if($post->user->subscribers->count() > 0)
                                    <button data-content-creator-id="{{$post->user->id}}"
                                            class="onlyfrogzUnsubscribe">{{__('onlyfrogz.unsubscribe')}}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="onlyfrogzSocial">
                        <button data-post-id="{{$post->id}}" class="onlyfrogzLikePost">{{$post->likes_count}} <i
                                class="{{$post->likes->count() > 0 ? "fas" : "far"}} fa-heart"></i></button>
                        <span>{{$post->comments_count}} <i class="far fa-comments"></i></span>
                        <button data-post-id="{{$post->id}}" class="onlyfrogzShare"><i class="fas fa-share-alt"></i>
                        </button>
                    </div>
                </div>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($post->contents as $content)
                            <div class="swiper-slide">
                                @switch($content->type)
                                    @case("video")
                                    <video src="{{$content->url}}" controls>
                                        @break
                                        @case("photo")
                                        <img src="{{$content->url}}">
                                    @break
                                @endswitch
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <a href="{{route('post',$post)}}">
                    <div class="onlyfrogzPostTextContainer">
                        <p class="onlyfrogzPostDescription">{{Str::limit($post->description,200)}}</p>
                        <div>
                            <div class="onlyfrogzTags">
                                @foreach($post->tags as $tag)
                                    <span style="background-color: {{$tag->color}}">{{$tag->name}}</span>
                                @endforeach
                            </div>
                            <h2 class="onlyfrogzPostTitle">{{$post->title}}
                                @if($post->premium)
                                    <i class="fas fa-star"></i>
                                @endif
                            </h2>
                        </div>
                    </div>
                </a>
            </article>
        @endforeach
    </div>
@endsection
