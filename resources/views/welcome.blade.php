@extends('layouts.onlyfrogz', ['wrapperClassname' => 'onlyfrogzBackgroundContent'])
@section('title',__('onlyfrogz.home'))
@section('content')
    <header class="onlyfrogzHeaderWelcome">
        <div class="onlyfrogzRightPanelWelcome">
            <h1 class="onlyfrogzTitleWelcome">OnlyFrogz</h1>
            <h2 class="onlyfrogzCatchphraseWelcome">{{__('onlyfrogz.catchphrase')}}</h2>
            <div class="onlyfrogzButtonsWelcome">
                <a class="onlyfrogzConnectionButtonWelcome" href="{{route('login')}}">{{__('onlyfrogz.connect')}}</a>
                <a href="{{route('register')}}">
                    <button class="onlyfrogzRegisterButtonWelcome">{{__('onlyfrogz.register')}}</button>
                </a>
            </div>
        </div>
    </header>
@endsection
