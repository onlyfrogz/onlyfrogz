@extends('layouts.onlyfrogz')
@section('title',__('onlyfrogz.user',['name'=>$user->name]))
@section('content')
    <header class="onlyfrogzUserInfoContainer">
        <div class="onlyfrogzCircleAvatarUser">
            <img src="{{$user->profile_photo_url}}"
                 alt="{{__('onlyfrogz.altprofilepicture',['name' => $user->name])}}">
        </div>
        <div class="onlyfrogzUserInfo">
            <h3 class="onlyfrogzUserName">{{$user->name}}</h3>
            <span><b>{{$user->subscribers_count}} </b>{{trans_choice('onlyfrogz.subscribers',$user->subscribers_count)}}</span>
            <span class="onlyfrogzPostCountInfo"><b>{{$user->posts_count}} </b>{{trans_choice('onlyfrogz.posts', $user->posts_count)}}</span>
            @if(!$hasValidSubscription && $user->id != Auth::user()->id)
                @if(Auth::user()->balance >= 30)
                    <select class="onlyfrogzSubscribeSelect" name="onlyfrogzSubscribeSelect">
                        <option selected value="1">{{trans_choice('onlyfrogz.subscribe',1)}}</option>
                        @if(Auth::user()->balance >= 75)
                            <option value="3">{{trans_choice('onlyfrogz.subscribe',3)}}</option>
                        @endif
                        @if(Auth::user()->balance >= 150)
                            <option value="6">{{trans_choice('onlyfrogz.subscribe',6)}}</option>
                        @endif
                    </select>
                    <button data-id="{{$user->id}}"
                            class="onlyfrogzConfirmSubscription">{{__('onlyfrogz.confirm')}}</button>
                @endif
            @else
                @if($user->subscribers->count() > 0)
                    <button data-content-creator-id="{{$user->id}}"
                            class="onlyfrogzUnsubscribe">{{__('onlyfrogz.unsubscribe')}}</button>
                @endif
            @endif
        </div>
    </header>
    <div class="onlyfrogzUserAllPosts">
        @foreach($user->posts as $post)
            <article class="onlyfrogzPostContainer">
                <?php
                $hasAccess = (!$post->premium || $hasValidSubscription || $user == Auth::user());
                ?>
                <div class="onlyfrogzAvatarAndSocialContainer">
                    <div class="onlyfrogzCircleAvatar">
                        <a href="{{route('user',$user)}}"><img src="{{$user->profile_photo_url}}"
                                                               alt="{{__('onlyfrogz.altprofilepicture',['name' => $user->name])}}"></a>
                        <div class="onlyfrogzUserInfoPanel">
                            <div>
                                <h3>{{$user->name}}</h3>
                                <span><b>{{$user->subscribers_count}} </b>{{trans_choice('onlyfrogz.subscribers',$user->subscribers_count)}}</span>
                                <span><b>{{$user->posts_count}} </b>{{trans_choice('onlyfrogz.posts', $user->posts_count)}}</span>
                                @if($user->subscribers->count() > 0 && $user->id != Auth::user()->id)
                                    <button data-content-creator-id="{{$user->id}}"
                                            class="onlyfrogzUnsubscribe">{{__('onlyfrogz.unsubscribe')}}</button>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="onlyfrogzSocial">
                        @if($hasAccess)
                            <button data-post-id="{{$post->id}}" class="onlyfrogzLikePost">{{$post->likes_count}} <i
                                    class="{{$post->likes->count() > 0 ? "fas" : "far"}} fa-heart"></i></button>
                            <span>{{$post->comments_count}} <i class="far fa-comments"></i></span>
                            <button data-post-id="{{$post->id}}" class="onlyfrogzShare"><i
                                    class="fas fa-share-alt"></i>
                            </button>
                        @endif
                    </div>
                </div>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        @foreach($post->contents as $content)
                            <div class="swiper-slide">
                                @switch($content->type)
                                    @case("video")
                                    <video src="{{$content->url}}"
                                           style="filter:{{ !$hasAccess ? 'blur(10px) saturate(0.3)': 'none'}};"
                                           {{!$hasAccess && "muted"}} controls>
                                        @break
                                        @case("photo")
                                        <img src="{{$content->url}}" alt=""
                                             style="filter:{{ !$hasAccess ? 'blur(10px) saturate(0.3)': 'none'}};">
                                    @break
                                @endswitch
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <a href="{{route('post',$post)}}">
                    <div class="onlyfrogzPostTextContainer">
                        @if($hasAccess)
                            <p class="onlyfrogzPostDescription">{{Str::limit($post->description,200)}}</p>
                        @else
                            <p class="onlyfrogzPostPremiumDescription">{{__('onlyfrogz.premium', ['name' => $user->name])}}</p>
                        @endif
                        <div>
                            <div class="onlyfrogzTags">
                                @foreach($post->tags as $tag)
                                    <span style="background-color: {{$tag->color}}">{{$tag->name}}</span>
                                @endforeach
                            </div>
                            <h2 class="onlyfrogzPostTitle">{{$post->title}}
                                @if($post->premium)
                                    <i class="fas fa-star"></i>
                                @endif
                            </h2>
                        </div>
                    </div>
                </a>
            </article>
        @endforeach
    </div>
@endsection
