<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Socialite\Contracts\User as ContractsUser;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{
    public function getOrCreateUser(ContractsUser $user)
    {
        $laravelUser = User::firstOrNew(['email' => $user->getEmail()]);
        if (!$laravelUser->exists) {
            $laravelUser->name = $user->getName() ? $user->getName() : $user->getNickname();
            $laravelUser->password = Hash::make(Str::random(16));
            $laravelUser->profile_photo_path = $user->getAvatar();
            $laravelUser->save();
        }
        return $laravelUser;
    }
    public function googleRedirect()
    {
        return Socialite::driver('google')->redirect();
    }
    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function twitterRedirect()
    {
        return Socialite::driver('twitter')->redirect();
    }
    public function handleGoogleLogin()
    {
        $google_user = Socialite::driver('google')->user();
        $laravelUser = $this->getOrCreateUser($google_user);

        Auth::login($laravelUser);
        return redirect('/feed');
    }
    public function handleFacebookLogin()
    {
        $fb_user = Socialite::driver('facebook')->user();
        $laravelUser = $this->getOrCreateUser($fb_user);

        Auth::login($laravelUser);
        return redirect('/feed');
    }
    public function handleTwitterLogin()
    {
        $twitter_user = Socialite::driver('twitter')->user();
        $laravelUser = $this->getOrCreateUser($twitter_user);

        Auth::login($laravelUser);
        return redirect('/feed');
    }
}
