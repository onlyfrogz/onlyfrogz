<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Like;
use App\Models\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Like $like
     * @return \Illuminate\Http\Response
     */
    public function show(Like $like)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Like $like
     * @return \Illuminate\Http\Response
     */
    public function edit(Like $like)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Like $like
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Like $like)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Like $like
     * @return \Illuminate\Http\Response
     */
    public function destroy(Like $like)
    {
        //
    }

    public function likePost(Request $request, Post $post)
    {
        $this->like($request, $post, 'posts');
    }

    public function likeComment(Request $request, Comment $comment)
    {
        $this->like($request, $comment, 'comments');
    }

    public function like(Request $request, Model $model, string $relation)
    {
        $direction = $request->input('direction');
        $like = Like::where('user_id', $request->user()->id)
            ->whereHas(
                $relation,
                function ($query) use ($model) {
                    return $query->where('id', $model->id);
                }
            )->first();
        if ($direction == 'like' && !$like) {
            $like = new Like();
            $like->user_id = $request->user()->id;
            $like->save();
            $model->likes()->attach($like);
        } elseif ($direction == 'unlike' && $like) {
            $like->delete();
        }
    }
}
