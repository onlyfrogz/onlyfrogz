<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use MeiliSearch\Endpoints\Indexes;

class SearchController extends Controller
{
    public function searchAll(Request $request)
    {
        if (!$request->input('q')) return;
        $posts = Post::search(
            $request->input('q'),
            function (Indexes $meiliSearch, string $query, array $options) {
                $options['limit'] = 30;
                return $meiliSearch->search($query, $options);
            }
        )->get();

        $posts = $posts->load(['user.subscribers' => function ($query) use ($request) {
            return $query->where('user_id', $request->user()->id);
        }, 'contents'])->filter(function ($val) {
            return !$val->premium || ($val->user->subscribers->get(0) && $val->user->subscribers->get(0)->pivot->expires_at > \Carbon\Carbon::now());
        })->take(6)->values();

        $users = User::search($request->input('q'))->get();

        return ["posts" => $posts, "users" => $users];
    }
}
