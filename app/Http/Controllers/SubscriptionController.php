<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Subscription $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Subscription $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Subscription $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscription $subscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Subscription $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        //
    }

    public function unsubscribe(User $user, Request $request)
    {
        $user->subscribers()->detach($request->user());
    }

    public function subscribe(User $user, Request $request)
    {
        if ($user->subscribers->contains($request->user())) {return;}
        $amountToCharge = match ($request->input('months')) {
            1 => 30,
            3 => 75,
            6 => 150,
            default => throw new \Exception('Invalid number of months'),
        };
        $request->user()->balance -= round($amountToCharge, 2);
        $request->user()->save();
        $user->balance += round($amountToCharge * 0.9, 2);
        $user->save();
        $user->subscribers()->attach($request->user(), ['expires_at' => Carbon::now()->addMonths($request->input('months'))]);
    }
}
