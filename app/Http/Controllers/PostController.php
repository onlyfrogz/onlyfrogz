<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePostRequest;
use App\Models\Content;
use App\Models\Post;
use App\Models\Tag;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $premium = $request->input('premium');
        $validated = $request->validated();
        $post = new Post();
        $post->title = $validated['title'];
        $post->description = $validated['description'];
        $post->premium = (bool)$premium;
        $tags = json_decode($validated['tags']);
        DB::transaction(function () use ($request, $tags, $post) {
            $request->user()->posts()->save($post);
            foreach ($tags as $tag) {
                $tagInstance = Tag::firstOrCreate(['name' => Str::lower($tag->value)]);
                $post->tags()->save($tagInstance);
            }
        });
        DB::transaction(function () use ($post, $request) {
            $i = 0;
            foreach ($request->file('file') as $file) {
                $url = Storage::put('public/contents', $file, 'public');
                $content = new Content();
                $content->position = $i;
                $content->url = $url;
                $mimeType = $file->getMimeType();
                switch ($mimeType) {
                    case Str::startsWith($mimeType, 'image'):
                        $contentType = 'photo';
                        break;
                    case Str::startsWith($mimeType, 'video'):
                        $contentType = 'video';
                        break;
                    default:
                        DB::rollBack();
                        throw new Exception('Invalid file type supplied');
                }
                $content->type = $contentType;
                $post->contents()->save($content);
                $i++;
            }
        });
        // Force refresh the user in the search index (mostly to fix invisible users that were not indexed)
        $request->user()->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }

    public function feedView(Request $request)
    {
        $subscriptions = $request->user()->subscriptions()->withCount(['posts', 'subscribers'])->get();
        $posts = Post::whereIn('user_id', $subscriptions->pluck('id'))->with(['user', 'contents', 'tags'])->with(['likes' => function ($query) use ($request) {
            return $query->where('user_id', $request->user()->id);
        }])->withCount(['likes', 'comments'])->orderBy('created_at', 'desc')->get();
        return view('feed', ['posts' => $posts, 'subscriptions' => $subscriptions]);
    }

    public function publishView(Request $request)
    {
        $posts = $request->user()->posts()->orderBy('created_at', 'desc')->with(['contents'])->get();
        return view('publish', ['posts' => $posts]);
    }

    public function discoverView(Request $request)
    {
        $tagsId = DB::table('post_tag')->select('tag_id')->join('posts', 'posts.id', '=', 'post_tag.post_id')->where('posts.premium', false)->groupBy('tag_id')->orderByDesc(DB::raw('count(tag_id)'))->limit(5)->get()->pluck('tag_id');
        $tags = Tag::whereIn('id', $tagsId)->with(['tenPosts' => function ($query) use ($request) {
            return $query->where('premium', false)->with(['likes' => function ($query) use ($request) {
                return $query->where('user_id', $request->user()->id);
            }])->withCount(['likes', 'comments']);
        }, 'tenPosts.contents', 'tenPosts.user' => function ($query) use ($request) {
            return $query->withCount(['posts', 'subscribers'])->with(['subscribers' => function ($query) use ($request) {
                return $query->where('users.id', $request->user()->id);
            }]);
        }])->get();
        $likes = DB::table('likes')->select('likeable_id', 'id')->join('likeables', 'likes.id', '=', 'likeables.like_id')->where('user_id', $request->user()->id)->get();
        $recommendedTagsIds = DB::table('tags')->select('id')->join('post_tag', 'tags.id', '=', 'post_tag.tag_id')->whereIn('post_tag.post_id', $likes->pluck('likeable_id'))->limit(5)->get()->pluck('id');
        $recommendedPosts = Post::whereHas('tags', function ($query) use ($recommendedTagsIds) {
            return $query->whereIn('id', $recommendedTagsIds);
        })->whereDoesntHave('likes', function ($query) use ($likes) {
            return $query->whereIn('id', $likes->pluck('id'));
        })->where('premium', false)->with(['likes' => function ($query) use ($request) {
            return $query->where('user_id', $request->user()->id);
        }, 'tags', 'contents', 'user' => function ($query) use ($request) {
            return $query->withCount(['posts', 'subscribers'])->with(['subscribers' => function ($query) use ($request) {
                return $query->where('users.id', $request->user()->id);
            }]);
        }])->withCount(['likes', 'comments'])->limit(10)->inRandomOrder()->get();
        return view('discover', ['tags' => $tags, 'recommendedPosts' => $recommendedPosts]);
    }

    public function postView(Post $post, Request $request)
    {
        $post->load(['contents', 'user' => function ($query) use ($request) {
            return $query->withCount(['posts', 'subscribers'])->with(['subscribers' => function ($query) use ($request) {
                return $query->where('user_id', $request->user()->id);
            }]);
        }, 'likes' => function ($query) use ($request) {
            return $query->where('user_id', $request->user()->id);
        }, 'tags', 'comments' => function ($query) use ($request) {
            return $query->with(['likes' => function ($query) use ($request) {
                $query->where('user_id', $request->user()->id);
            }, 'user' => function ($query) use ($request) {
                return $query->withCount(['posts', 'subscribers'])->with(['subscribers' => function ($query) use ($request) {
                    $query->where('user_id', $request->user()->id);
                }]);
            }])->orderByDesc('created_at')->withCount(['likes']);
        }])->loadCount(['likes', 'comments']);
        $hasValidSubscription = ($post->user->subscribers->count() > 0 && Carbon::now() < $post->user->subscribers->first()->pivot->expires_at);
        return view('post', ['post' => $post, 'hasValidSubscription' => $hasValidSubscription]);
    }

    public function deletePost(Request $request, Post $post)
    {
        DB::transaction(function () use ($post) {
            $post->comments()->delete();
            $post->tags()->detach();
            $post->contents()->delete();
            $post->delete();
        });
        $request->user()->save();
    }
}
