<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function userView(User $user, Request $request)
    {
        $user->load(['posts' => function ($query) {
            return $query->latest()->limit(10)->withCount(['comments', 'likes']);
        }, 'posts.contents', 'subscribers' => function($query) use ($request) {
            return $query->where('user_id', $request->user()->id);
        }, 'posts.likes' => function ($query) use ($request) {
            return $query->where('user_id', $request->user()->id);
        }, 'posts.tags'])->loadCount(['subscribers', 'posts']);
        $hasValidSubscription = ($user->subscribers->count() > 0 && Carbon::now() < $user->subscribers->first()->pivot->expires_at);
        return view('user', ['user' => $user, 'hasValidSubscription' => $hasValidSubscription]);
    }
}
