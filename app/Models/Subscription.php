<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class Subscription extends Pivot
{
    public $incrementing = true;

    protected $attributes = [
        'expires_at' => 0
    ];
}
