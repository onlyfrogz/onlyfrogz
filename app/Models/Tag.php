<?php

namespace App\Models;

use Colors\RandomColor;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;

    public static function boot()
    {
        parent::boot();
        static::saving(function($tag){
            if(!isset($tag->attributes['color']))  {
                $tag->attributes['color'] = RandomColor::one(array(
                    'luminosity' => 'dark'
                ));;
            }
        });
    }

    protected $fillable = [
        'name'
    ];

    public function posts(){
        return $this->belongsToMany(Post::class);
    }

    public function tenPosts(){
        return $this->posts()->latest()->limit(10);
    }
}
