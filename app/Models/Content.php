<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Content extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $attributes = [
        'url' => '',
        'type' => '',
    ];

    protected $fillable = [
        'position'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function getUrlAttribute($value)
    {
        if (isset(parse_url($value)["scheme"])) return $value;
        else return Storage::url($value);
    }
}
