<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Laravel\Scout\Searchable;

class Post extends Model
{
    use HasFactory;
    use Searchable;
    use \Staudenmeir\EloquentEagerLimit\HasEagerLimit;

    protected $attributes = [
        'description' => '',
        'premium' => false,
    ];

    protected $fillable = [
        'title'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function contents()
    {
        return $this->hasMany(Content::class);
    }

    public function likes()
    {
        return $this->morphToMany(Like::class, 'likeable');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        $array["tags"] = $this->tags()->get()->map->only(['name', 'color'])->toArray();

        return $array;
    }
}
