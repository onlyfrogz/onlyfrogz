<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignId('user_id')->constrained();
            $table->mediumText('title');
            $table->mediumText('description');
            $table->boolean('premium');
            $table->softDeletes();
        });
        Schema::create('contents', function (Blueprint $table){
           $table->id();
           $table->foreignId('post_id')->constrained();
           $table->integer('position');
           $table->mediumText('url');
           $table->enum('type',['photo','video','audio']);
           $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
        Schema::dropIfExists('posts');
    }
}
