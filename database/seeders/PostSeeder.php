<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Content;
use App\Models\Like;
use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elTagito = Tag::factory()->create(['name' => 'Le tag que tout le monde aura']);
        $elPostito = User::factory()->create(['name' => 'Serial poster']);
        Post::factory()->count(200)->for($elPostito)->hasAttached($elTagito)->has(Content::factory()->count(3)->state(new Sequence(
            ['position' => 0],
            ['position' => 1],
            ['position' => 2]
        )))->create();
        Post::factory()->count(20)
            ->for(User::factory())
            ->has(Tag::factory())
            ->hasAttached($elTagito)
            ->hasTags(2)
            ->has(Comment::factory()->count(5)->state(new Sequence(
                fn () => ['user_id' => User::all()->random()]
            )))
            ->has(Like::factory()->count(10)->state(new Sequence(
                fn () => ['user_id' => User::all()->random()]
            )))
            ->has(Content::factory()->count(3)->state(new Sequence(
                ['position' => 0],
                ['position' => 1],
                ['position' => 2]
            )))
            ->create();
    }
}
